import { get } from "svelte/store"
import { brands as brandsStore, types as typesStore } from "@/stores/static"

export function match(param) {
	const brands = get(brandsStore)
	const inBrands = brands.find(({ url }) => url === param)

	const types = get(typesStore)
	const inTypes = types.find(({ url }) => url === param)

	return inTypes || inBrands
}