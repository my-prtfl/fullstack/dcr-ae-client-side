export default {
	head_row: {
		tags: [ "Dubai car rental", "9-year track record of success" ],
		title: "ALL THE WORLD'S BEST CARS — ALL IN THE DCR FLEET!"
	},
	choice: {
		title: "WHEN YOU RENT A CAR FROM DCR, IT'S YOUR CHOICE!",
		text: {
			0: "Traveling about the amazing, fabulous United Arab Emirates in gorgeous car—wouldn't that be cool?! You will certainly be pleased by our service. Contact us and see for yourself!",
			1: "Dubai Car Rental has a large fleet of cars. We are happy to provide you with luxury cars, supercars, and SUVs from the most prestigious giants of the automotive industry. Want a Lamborghini?<br/>You got it! Would you prefer a Bentley or Ferrari? No problem!",
			2: "Our inventory includes more than 50 prestigious cars with famous brands.",
			3: "All of them are in immaculate condition. For your convenience, they are equipped with GPS and include a full tank of gas!"
		}
	}
}