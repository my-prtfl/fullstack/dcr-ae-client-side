export default {
	driver: {
		title: "PERSONAL DRIVER",
		content: "If you would prefer to simply relax and enjoy the trip rather than sit behind the wheel, then our chauffeur services are for you."
	},
	concierge: {
		title: "PERSONAL CONCIERGE",
		content: "Whether you need help arranging business or personal affairs, or you simply want to enjoy your vacation in beautiful Dubai, a personal concierge is at your service."
	},
	vacation: {
		title: "VACATION PLANNING",
		content: "Want to celebrate your wedding anniversary in the luxurious city of Dubai? Want to ring in the New Year in a real-life fairy tale? Want to plan an unforgettable romantic date for your girlfriend? We will quickly arrange a visa for you, book your hotel, and help you plan unsurpassed leisure activities. We guarantee a wonderful time and pleasant memories for life!"
	},
	limousine: {
		title: "CHAUFFEURED LIMOUSINE RENTALS FOR VIPS",
		content: "Want to feel like a real elite? Want to play the part of a millionaire in the best city in the world? Then this service is for you. You can rent chauffeured limousines, SUVs, business and luxury class cars at hourly rates. A true luxury for those who want to be the best!"
	}
}