export default {
	nav: {
		main: "Main page",
		inventory: "Car inventory",
		price: "Price list",
		services: "Services",
		blog: "Blog",
		faq: "FAQ",
		terms: "Terms and conditions",
		about_us: "About us",
		sitemap: "Sitemap"
	},
	footer: {
		help: "You can contact us for help directly via feedback.<br/>We will be happy to help you!",
		licence: "Licence № {num}"
	},
	choice: "IT'S YOUR CHOICE!",
	crumbs: {
		homepage: "Luxury car rental Dubai"
	},
	no_results: "No results for your search",
	selected: "Selected {num} cars",
	tags: {
		price: "Price {url}"
	},
	contacts: "Contacts",
	404: "Oh no! The page you are looking for has been rented and left!"
}