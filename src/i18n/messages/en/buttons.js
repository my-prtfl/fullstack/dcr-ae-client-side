export default {
	order_car: "Order car",
	order: "Order",
	select_car: "Select car",
	see_all: "See all",
	select_brand: "Select a brand",
	close: "Close",
	popular: "Popular",
	filter: "Filter",
	clear_all: "Clear all",
	download_price: "Download price list"
}