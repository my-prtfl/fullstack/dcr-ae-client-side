export default {
	negotiable: "Negotiable",
	desc: "Description",
	specs: "Specifics",
	years: "9-year",
	track_record: "track record of success",
	discount: "Discount",
	for_rent: "for rent from 3 days",
}