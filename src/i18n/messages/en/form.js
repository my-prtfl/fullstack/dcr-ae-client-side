export default {
	order: "Order {name}",
	name: "Your name",
	phone: "Phone number",
	start: "Start of lease:",
	end: "End of lease:",
	contact_by: "Contact by",
	call: "Phone call",
	thanks: "We have received your order and will contact you shortly",
	search: "Search"
}