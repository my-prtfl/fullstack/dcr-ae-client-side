export default {
	rent: "The longer you rent a car, the better your price.",
	all_prices: "All prices are shown without VAT. The tax is effective from January 1, 2018 and is 5% of the rental price.",
	content: [
		"Dubai Car Rental has been in business since 2013. Since that time, it has established itself as a successful transport services company in the UAE.",
		"Our main business is renting cars that fit each customer's preferences.",
		"We have raised the quality of our services to the highest levels, thereby earning an impeccable reputation among our customers."
	]
}