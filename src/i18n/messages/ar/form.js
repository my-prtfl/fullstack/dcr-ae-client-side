export default {
	order: "طلب {name}",
	name: "اسمك",
	phone: "رقم الهاتف",
	start: "بداية عقد الإيجار:",
	end: "نهاية عقد الإيجار:",
	contact_by: "الاتصال عن طريق",
	call: "مكالمة هاتفية",
	thanks: "لقد تلقينا طلبك وسنتصل بك قريبًا",
	search: "يبحث"
}