export default {
	by_make: "عن طريق الصنع",
	by_type: "حسب النوع",
	recently: "شوهدت مؤخرا",
	popular: "السيارات الشعبية",
	more: "المزيد {{name}}",
	all_cars: "جميع السيارات",
	pages: "الصفحات",
	blog: "مقالات",
	classes: "الطبقات",
	brands: "العلامات التجارية",
	cars: "سيارات",
	class: "فصل",
	make: "يجعلون",
	price: "سعر",
	read_more: "اقرأ أكثر",
	pricelist: "قـائـمـة بـأسـعـار جـمـيـع أنـواع الـسـيـارات في مـركـز دبي لـلـتـأجـير DCR",
	faq: "الأسئلة الشائعة حول أفضل تأجير أسطول سيارات في دبي",
	blog: "أفضل مدونة لتأجير السيارات في دبي",
	try_it: "جربه هنا",
	back: "خلف"
}