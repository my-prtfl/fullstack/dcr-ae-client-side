export default {
	negotiable: "قـابـل لـلـتـفـاوض",
	desc: "وصف",
	specs: "تفاصيل",
	years: "9 سنوات",
	track_record: "من العمل الناجح.",
	discount: "خصم",
	for_rent: "للإيجار من 3 أيام"
}