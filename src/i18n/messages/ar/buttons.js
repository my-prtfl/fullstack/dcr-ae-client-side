export default {
	order_car: "استئجار سيارة",
	order: "ترتيب",
	select_car: "اختر السيارة",
	see_all: "اظهار الكل",
	select_brand: "اختر علامة تجارية",
	close: "قريب",
	popular: "بشعبية",
	filter: "منقي",
	clear_all: "امسح الكل",
	download_price: "تنزيل قائمة الأسعار"
}