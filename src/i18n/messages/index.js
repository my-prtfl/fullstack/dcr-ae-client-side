const imports = {
	...import.meta.glob("./ru/*.js"),
	...import.meta.glob("./en/*.js"),
	...import.meta.glob("./ar/*.js")
}

const data = []

for (let prop in imports) {
	const key = prop.replace(/.\/(ru|en|ar)\/([^.]*).js/, "$2")
	const locale = prop.replace(/.\/(ru|en|ar)[^.]*.js/, "$1")

	data.push({
		locale,
		key,
		loader: async () => (
			await import(`./${locale}/${key}.js`)
		).default
	})
}

export default data