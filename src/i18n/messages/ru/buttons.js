export default {
	order_car: "Заказать авто",
	order: "Заказать",
	select_car: "Выбрать авто",
	see_all: "Смотреть все",
	select_brand: "Выбрать бренд",
	close: "Закрыть",
	popular: "По популярности",
	filter: "Фильтр",
	clear_all: "Очистить всё",
	download_price: "Скачать прайс-лист"
}