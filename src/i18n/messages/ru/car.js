export default {
	negoitable: "Договорная",
	desc: "Описание",
	specs: "Спецификации",
	years: "9 лет",
	track_record: "успешной работы",
	discount: "Скидка",
	for_rent: "на аренду от 3 дней"
}