import I18n from "@sveltekit-i18n/base"
import parser from "@sveltekit-i18n/parser-icu"

import { derived } from "svelte/store"

import loaders from "./messages"

const config = ({ 
	parser: parser(),
	loaders
})


const i18n = new I18n(config)

const { t, locale, locales, loading, translations, loadTranslations, setLocale } = i18n

const l = derived(
	locale,
	$locale => (pathname, loc) => {
		if (!pathname && pathname !== "") return false

		loc = loc || $locale
		
		if (loc == "en") return `/${pathname}`
		return `/${loc}/${pathname}`
	}
)

export { t, l, locale, locales, loading, translations, loadTranslations, setLocale }