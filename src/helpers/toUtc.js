import dayjs from "dayjs"
import utc from "dayjs/plugin/utc"

dayjs.extend(utc)

export default data => {
	const date = dayjs(data).format("YYYY-MM-DD")
	const time = dayjs(data).format("HH:mm:ss")

	return dayjs.utc(`${date}${time}`).format()
}