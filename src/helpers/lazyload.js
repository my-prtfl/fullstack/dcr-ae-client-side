import LazyLoad from "vanilla-lazyload"
import { tick } from "svelte"

const initLazyLoad = () => {
	let instance = null

	if (!import.meta.env.SSR)
		instance = new LazyLoad({
			elements_selector: ".lazy"
		})

	return instance
}

const instance = initLazyLoad()

export const update = async () => {
	if (import.meta.env.SSR) return

	instance.restoreAll()
	await tick()
	instance.loadAll()
}

export default instance