const getExt = name => {
	let parts = name.split('.')
	return parts[parts.length - 1]
}

const getName = name => {
	let ext = getExt(name)
	return name.replace(new RegExp(`.${ext}$`), "")
}

export default name => [ "1440xw", "400x", "x85" ].reduce((a, v) => ({
	...a,
	[v]: `/files/products/${getName(name)}.${v}.${getExt(name)}`
}), {})