import { writable } from "svelte/store"

let index = writable(0)

export const setIndex = svg => {
	let i
	index.subscribe(val => i = val)

	const rect = svg.querySelector("rect")
	const fill = rect?.getAttribute("fill")

	if (rect && fill && /url\(#pattern[0-9]*\)/.test(fill))  {
		const pattern = svg.querySelector("pattern")

		rect.setAttribute("fill", `url(#pattern${i})`)
		pattern.setAttribute("id", `pattern${i}`)

		i++
		index.set(i)
	}

	return svg
}