export default ctx => {
	const pathArr = ctx.url?.pathname?.split("/")

	let localePath = pathArr[1]

	if (!["ru", "ar"].includes(localePath)) localePath = "en"

	const selectedLocale = localePath || ctx.cookies?.get("i18n")
	const preferedLocale = ctx.request?.headers?.get("accept-language")?.split(",")[1]

	const locale = selectedLocale || preferedLocale?.split(";")[0] || "en"
	
	return locale
}