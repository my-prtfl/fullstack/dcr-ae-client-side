import { types, brands } from "@/stores/static"
import { getTypes } from "@/sequelize/getTypes"
import { getBrands } from "@/sequelize/getBrands"
import localeChecker from "@/helpers/localeChecker"
import dayjs from "dayjs"

let lastUpdate

const update = async () => {
	types.set( await getTypes() )
	brands.set( await getBrands() )
	lastUpdate = dayjs()

	console.log("Types and brands updated")
}

/** @type {import('@sveltejs/kit').Handle} */
export async function handle({ event, resolve }) {
	let diff = dayjs().diff(lastUpdate, "hour")
	
	if (diff >= 1 || !lastUpdate) await update()
 
	const response = await resolve(event, {
		transformPageChunk: ({ html }) => html.replace('%lang%', localeChecker(event))
	})
	
	return response
}