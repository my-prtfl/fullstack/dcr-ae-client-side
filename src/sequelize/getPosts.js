import db from "@/sequelize"

export const getPosts = async () => {
	const query = `
		SELECT
			b.url,
			JSON_OBJECT(
				'en', b.text,
				'ru', b.text_ru,
				'ar', b.text_ar
			) as body,
			JSON_OBJECT(
				'en', b.meta_title,
				'ru', b.meta_title_ru,
				'ar', b.meta_title_ar
			) as name
		FROM s_blog as b
		WHERE b.visible = 1
	`

	const [posts] = await db.query(query)

	for (let i in posts) {
		const post = posts[i]

		const reg = /<img src="(.*?)" (.*?) \/>/
		const src = post.body.en.match(reg)[1]

		posts[i].promo = src
	}

	return posts
}

export const getPost = async url => {
	const query = `
		SELECT
			b.name,
			b.url,
			JSON_OBJECT(
				'en', b.text,
				'ru', b.text_ru,
				'ar', b.text_ar
			) as body,
			JSON_OBJECT(
				'en', b.meta_title,
				'ru', b.meta_title_ru,
				'ar', b.meta_title_ar
			) as name
		FROM s_blog as b
		WHERE 
			b.visible = 1
			AND b.url = '${url}'
	`

	const moreQuery = `
		SELECT
			b.name,
			b.url,
			JSON_OBJECT(
				'en', b.text,
				'ru', b.text_ru,
				'ar', b.text_ar
			) as body,
			JSON_OBJECT(
				'en', b.meta_title,
				'ru', b.meta_title_ru,
				'ar', b.meta_title_ar
			) as name
		FROM s_blog as b
		WHERE 
			b.visible = 1
			AND b.url <> '${url}'
		ORDER BY RAND()
		LIMIT 4
	`
	
	const [ [[post]], [more] ] = await Promise.all([
		db.query(query),
		db.query(moreQuery)
	])

	for (let i in more) {
		const post = more[i]

		const reg = /<img src="(.*?)" (.*?) \/>/
		const src = post.body.en.match(reg)[1]

		more[i].promo = src
	}

	return {post, more}
}