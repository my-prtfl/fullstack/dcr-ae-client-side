import db from "@/sequelize"

export const getPage = async slug => {
	const query = `
		SELECT
			JSON_OBJECT(
				'en', p.body,
				'ru', p.body_ru,
				'ar', p.body_ar
			) as body
		FROM s_pages as p
		WHERE p.url = '${slug}'
	`

	const [[page]] = await db.query(query)
	
	return page?.body
}

export const getList = async () => {
	const query = `
		SELECT url
		FROM s_pages as p
		WHERE p.visible = 1
	`

	const [pages] = await db.query(query)

	return pages
}