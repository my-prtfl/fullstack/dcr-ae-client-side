import "dotenv/config"

import { Sequelize } from "sequelize"

const connection = new Sequelize(
	process.env.DB_NAME,
	process.env.DB_USERNAME,
	process.env.DB_PASSWORD,
	{
		host: process.env.DB_URL,
		dialect: 'mysql',
		logging: process.env.NODE_ENV !== "development" ? false : (sql, timingMs, l) => console.log(`SQL - [Execution time: ${timingMs}ms]`),
		benchmark: process.env.NODE_ENV === "development"
	}
)

export default connection