import db from "@/sequelize"

export const getTypes = async () => {
	const query = `
		SELECT name, url
		FROM s_categories
		WHERE visible = 1
		ORDER BY position
	`

	const [types] = await db.query(query)

	return types
}