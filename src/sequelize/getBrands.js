import { brands } from "@/stores/static"

import db from "@/sequelize"

export const getBrands = async () => {
	const query = `
		SELECT *
		FROM (
			SELECT 
				name, url, position,
				( 
					SELECT
						COUNT(*)
					FROM s_products as c
					WHERE 
						c.brand_id = b.id
						AND c.visible = 1
				) as cars
			FROM s_brands as b
		) as sub
		WHERE sub.cars > 0
		ORDER BY sub.position
	`
	
	const [brands] = await db.query(query)

	return brands
}

// const fetch = async () => brands.set( await getBrands() ); console.log("Brands upadted");
// fetch()
// setInterval(fetch, 1000 * 60 * 60)