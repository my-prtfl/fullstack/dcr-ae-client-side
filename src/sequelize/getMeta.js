import db from "@/sequelize"

export const getMeta = async (url, table, locale = "") => {
	let postfix = locale ? `_${locale}` : ""

	const query = `
		SELECT
			meta_title${postfix} as title,
			meta_description${postfix} as description,
			meta_keywords${postfix} as keywords
		FROM s_${table}
		WHERE url = '${url}'
	`

	const [[meta]] = await db.query(query)

	if (table === "categories") {
		switch(locale) {
			case "ar":
				meta.title = meta.title + " | دي سي آر (دبي لتأجير السيارات)"
				meta.description = "تأجير " + meta.title + "سجل نجاح مدته 5 سنوات جميع أفضل السيارات في العالم - كل ما في أسطول DCR! استأجر سيارة لمدة 3 أيام أو أكثر واحصل على خصم. ☎ تعال واستأجر سيارة!"
				meta.keywords = `تأجير ${meta.title} ، استئجار ${meta.title} ، تأجير ${meta.title} في دبي ، تأجير ${meta.title} في الإمارات ، استئجار ${meta.title} في دبي ، استئجار ${meta.title} في الإمارات ، تأجير ${meta.title} ، استئجار ${meta.title} ، تأجير ${meta.title} في دبي ، تأجير ${meta.title} في دبي ، تأجير ${meta.title} في الإمارات العربية المتحدة ، استئجار ${meta.title} في الإمارات العربية المتحدة`
				break;
			case "ru":
				meta.title = "Аренда " + meta.title + " в Дубае | DCR (Dubai Car Rental)"
				meta.description = "Аренда " + meta.title + ". 5 лет успешной работы. Все лучшие автомобили мира - в одном автопарке DCR. Скидка при аренде от 3-х дней. ☎ Заходите!"
				meta.keywords = `аренда ${meta.title}, прокат ${meta.title}, аренда ${meta.title} в Дубае, аренда ${meta.title} в ОАЭ, прокат ${meta.title} в Дубае, прокат ${meta.title} в ОАЭ`
				break;
			default:
				meta.title = "Rent " + meta.title + " cars in Dubai | DCR (Dubai Car Rental)"
				meta.description = "Rent " + meta.title + ". 5-year track record of success All the world's best cars - all in the DCR fleet! Rent a car for 3 days or more and get a discount. ☎ Come and rent a car!"
				meta.keywords = `rent ${meta.title}, hire ${meta.title}, rent ${meta.title} in Dubai, rent ${meta.title} in the UAE, hire ${meta.title} in Dubai, hire ${meta.title} in the UAE, rent ${meta.title}, hire ${meta.title}, rent ${meta.title} in Dubai, hire ${meta.title} in Dubai, rent ${meta.title} in the UAE, hire ${meta.title} in the UAE`
				break;
		}
	}

	return {
		...meta,
		table
	}
}