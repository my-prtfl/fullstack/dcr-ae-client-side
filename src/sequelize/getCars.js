import db from "@/sequelize"

const variants = {
	"Бензин": { en: "Petrol", ar: "بنزين" },
	"Дизель": { en: "Diesel", ar: "ديزل" },
	"км/ч": { en: "km/h", ar: "كم / ساعة" },
	"есть": { en: "Yes", ar: "نعم فعلا" },
	"АИ-": { en: "", ar: "" },
	"Радио": { en: "Radio", ar: "راديو" },
	"Полный": { en: "Full", ar: "ممتلئ" },
	"АКПП": { en: "Automatic", ar: "تلقائي" },
	"РКПП": { en: "Manual", ar: "كتيب" },
	"Задний": { en: "Rear wheel drive", ar: "دفع خلفي" },
	"Передний": { en: "Front wheel drive", ar: "نظام دفع بالعجلات الأمامية" },
	"секунды": { en: "seconds", ar: "ثواني" },
	"секунд": { en: "seconds", ar: "ثواني" },
	"сек": { en: "seconds", ar: "ثواني" },
	"л.с.": { en: "HP", ar: "HP" },
	"места": { en: "seats", ar: " المقاعد" },
	"мест": { en: "seats", ar: " المقاعد" },
	" л": { en: "", ar: "" },
}

const replace = (str, locale = "en") => {
	for (let key in variants) {
		str = str.replace(key, variants[key][locale])
	}
	return str
}

const setOptions = (options, locale) => {
	if (!options) return []

	for (let i in options) {
		const { value } = options[i]
		
		switch(locale) {
			case "ar":
				options[i].value = replace(value, "ar")
				break;
			default:
				options[i].value = replace(value)
				break;
		}
	}

	return options
}

export const getCars = async (params = {}) => {
	let where = ""
	const select = `
	`

	if (params.types)
		where += `AND cat.url IN('${params.types.split(",").join("','")}')`

	if (params.brands)
		where += `AND b.url IN('${params.brands.split(",").join("','")}')`

	if (params.price_min)
		where += `AND v.usd >= ${parseFloat(params.price_min)?.toFixed(2) || 0}`
	
	if (params.price_max)
		where += `AND v.usd <= ${parseFloat(params.price_max)?.toFixed(2) || 0}` 

	const query = `
		SELECT
			c.name, c.url,
			v.usd AS price,
			JSON_ARRAYAGG(
				JSON_OBJECT(
					'filename', i.filename,
					'position', i.position
				)
			) as images,
			JSON_OBJECT(
				'name', b.name,
				'url', b.url
			) as brand
		FROM
			s_products AS c,
			s_images AS i,
			s_products_categories as pcat,
			s_categories as cat,
			( SELECT price, product_id, position, TRUNCATE(price / 3.64963504, 2) as usd FROM s_variants ) as v,
			s_brands AS b
		WHERE
			c.visible = 1
			AND i.product_id = c.id
			AND pcat.product_id = c.id
			AND pcat.category_id = cat.id
			AND v.product_id = c.id
			AND c.brand_id = b.id
			${where || ""}
		GROUP BY c.id, v.usd, v.price
		ORDER BY price DESC
	`
	
	const [ cars ] = await db.query(query)
	
	return cars
}

export const getPopular = async () => {
	const select = `
		(
			SELECT
				COUNT(*)
			FROM s_purchases as p
			WHERE
				c.id = p.product_id
		) as orders,
	`

	const query = `
		SELECT
			c.name, c.url,
			v.usd AS price,
			JSON_ARRAYAGG(
				JSON_OBJECT(
					'filename', i.filename,
					'position', i.position
				)
			) as images,
			JSON_OBJECT(
				'name', b.name,
				'url', b.url
			) as brand,
			(
				SELECT COUNT(*)
				FROM s_purchases as p
				WHERE c.id = p.product_id
			) as orders
		FROM
			s_products AS c,
			s_images AS i,
			s_products_categories as pcat,
			s_categories as cat,
			( SELECT price, product_id, TRUNCATE(price / 3.64963504, 2) as usd FROM s_variants ) as v,
			s_brands AS b
		WHERE
			c.visible = 1
			AND i.product_id = c.id
			AND pcat.product_id = c.id
			AND pcat.category_id = cat.id
			AND v.product_id = c.id
			AND c.brand_id = b.id
			AND c.featured = 1
		GROUP BY c.id, v.usd, v.price
		ORDER BY rand()
		LIMIT 12
	`

	const [cars] = await db.query(query)

	return cars
}

export const getCar = async (slug, locale = "") => {
	const where = `AND c.url = '${slug}'`

	const query = `
		SELECT
			c.name, c.url,
			v.usd AS price,
			v.id AS variant_id,
			JSON_ARRAYAGG(
				JSON_OBJECT(
					'filename', i.filename,
					'position', i.position
				)
			) as images,
			JSON_OBJECT(
				'name', b.name,
				'url', b.url
			) as brand,
			JSON_OBJECT(
				'en', body,
				'ru', body_ru,
				'ar', body_ar
			) as body,
			(
				SELECT
					JSON_ARRAYAGG(
						JSON_OBJECT(
							'name', opts.oname,
							'value', opts.value
						)
					)
				FROM (
					SELECT o.product_id, o.value, JSON_OBJECT('en', f.name, 'ru', f.name_ru, 'ar', f.name_ar) as oname FROM s_options as o INNER JOIN s_features as f on o.feature_id = f.id
				) as opts
				WHERE opts.product_id = c.id
			) as options
		FROM
			s_products AS c,
			s_images AS i,
			( SELECT id, price, product_id, TRUNCATE(price / 3.64963504, 2) as usd FROM s_variants ) as v,
			s_brands AS b
		WHERE
			c.visible = 1
			AND i.product_id = c.id
			AND v.product_id = c.id
			AND c.brand_id = b.id
			AND c.url = '${slug}'
		GROUP BY c.id, v.id, v.usd, v.price
	`

	const moreQuery = `
		SELECT
			c.name, c.url,
			v.usd AS price,
			JSON_ARRAYAGG(
				JSON_OBJECT(
					'filename', i.filename,
					'position', i.position
				)
			) as images,
			JSON_OBJECT(
				'name', b.name,
				'url', b.url
			) as brand,
			(
				SELECT COUNT(*)
				FROM s_purchases as p
				WHERE c.id = p.product_id
			) as orders
		FROM
			s_products AS c,
			s_images AS i,
			s_products_categories as pcat,
			s_categories as cat,
			( SELECT price, product_id, TRUNCATE(price / 3.64963504, 2) as usd FROM s_variants ) as v,
			s_brands AS b
			
		INNER JOIN (
			SELECT b.id
			FROM s_products as p
			INNER JOIN s_brands as b
				ON b.id = p.brand_id
			WHERE p.url = '${slug}'
		) as brand

		WHERE
			c.visible = 1
			AND i.product_id = c.id
			AND pcat.product_id = c.id
			AND pcat.category_id = cat.id
			AND v.product_id = c.id
			AND c.brand_id = b.id
			AND c.brand_id = brand.id
			AND c.url <> '${slug}'
			
		GROUP BY c.id, v.usd, v.price
		ORDER BY orders DESC
	`

	const [ [[car]], [more] ] = await Promise.all([
		db.query(query),
		db.query(moreQuery)
	])

	if (car)
		car.options = setOptions(car.options, locale)

	return { car, more }
}

export const getPrice = async () => {
	const query = `
		SELECT
			b.name, b.url,
			JSON_ARRAYAGG(JSON_OBJECT(
				'name', c.name,
				'price', JSON_OBJECT(
					'aed', v.price,
					'usd', v.usd
				),
				'types', c.types,
				'url', c.url
			)) AS cars
		FROM
			s_brands as b,
			(
				SELECT
					c2.*,
					JSON_ARRAYAGG(cat.value) as types
				FROM
					s_products as c2,
					( 
						SELECT
							spc.product_id,
							JSON_OBJECT(
								'name', sc.name,
								'url', sc.url,
								'position', spc.position
							) as value
						FROM 
							s_products_categories as spc,
							s_categories as sc
						WHERE spc.category_id = sc.id
						ORDER BY spc.position ASC
					) AS cat
				WHERE cat.product_id = c2.id
				GROUP BY id
				ORDER BY c2.position ASC
			) as c,
			( SELECT price, product_id, TRUNCATE(price / 3.64963504, 2) as usd FROM s_variants ) as v
		WHERE
			c.visible = 1
			AND v.product_id = c.id
			AND c.brand_id = b.id
		GROUP BY name, url, b.position
		ORDER BY b.position ASC
	`

	const [cars] = await db.query(query)

	return cars
}

export const getList = async () => {
	const query = `
		SELECT
			name, url
		FROM s_products
		WHERE visible = 1
		ORDER BY name ASC
	`

	const [cars] = await db.query(query)

	return cars
}

export const getCommerce = async () => {
	/*
	DROP FUNCTION IF EXISTS imagename;
		
	CREATE FUNCTION imagename( starting_value TEXT )
	RETURNS TEXT
	
	BEGIN
	
		DECLARE result TEXT;
	
		SET result = REPLACE(starting_value, '.jpg', '.1440xw.jpg');
		SET result = REPLACE(result, '.jpeg', '.1440xw.jpeg');
	SET result = REPLACE(result, '.png', '.1440xw.jpg');
	
		RETURN CONCAT('https://dcr.ae/files/products/', result);
	
	END;

	*/

	const query = `
		SELECT
			c.id,
			c.name as title,
			c.body as description,
			'available for order' as availability,
			'new' as "condition",
			CONCAT(v.usd, ' USD') AS price,
			CONCAT('https://dcr.ae/cars/', c.url) as link,
			imagename(i.filename) as image_link,
			b.name as brand
		FROM
			s_products AS c,
			s_images AS i,
			s_products_categories as pcat,
			( SELECT * FROM s_categories WHERE s_categories.visible = 1 ) as cat,
			( SELECT price, product_id, position, TRUNCATE(price / 3.64963504, 2) as usd FROM s_variants ) as v,
			s_brands AS b
		WHERE
			c.visible = 1
			AND i.product_id = c.id
			AND pcat.product_id = c.id
			AND pcat.category_id = cat.id
			AND v.product_id = c.id
			AND c.brand_id = b.id
		GROUP BY c.id, v.usd, v.price
		ORDER BY c.position ASC
	`

	const [cars] = await db.query(query)

	return cars
}