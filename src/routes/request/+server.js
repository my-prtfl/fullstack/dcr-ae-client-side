import "dotenv/config"

import axios from "axios"
import FormData from "form-data"

/** @type {import("./$types").RequestHandler} */
export async function POST(e) {
	const body = await e.request.json()
	
	const form = new FormData()

	for (let prop in body) {
		if (typeof body[prop] !== "object")
			form.append(prop, body[prop])
		else
			form.append(prop, JSON.stringify(body[prop]))
	}

	await axios.post(`https://${process.env.ADMIN_URL}/ajax/order.php`, form)

	return new Response(200)
}