import { decode } from "html-entities"
import { Parser } from "json2csv"

import { getCommerce } from "@/sequelize/getCars"

export async function GET() {
	const cars = await getCommerce()

	for (let i in cars) {
		const car = cars[i]
		cars[i].description = decode(car.description.replace(/(<([^>]+)>)/ig, ""))
	}

	const fields = [
		"id",
		"title",
		"description",
		"availability",
		"condition",
		"price",
		"link",
		"image_link",
		"brand"
	]

	const parser = new Parser({ fields })

	const body = parser.parse(
		cars.map(car => ({ ...car }))
	)
	
	const headers = {
    'Cache-Control': `max-age=0, s-max-age=${600}`,
    'Content-Type': 'text/csv',
		'Content-Disposition': 'attachment;filename=commerce.csv'
  }

	return new Response(
		body,
		{ headers }
	)
}