import dayjs from "dayjs"

import { getTypes } from "@/sequelize/getTypes"
import { getBrands } from "@/sequelize/getBrands"
import { getList } from "@/sequelize/getCars"
import { getPosts } from "@/sequelize/getPosts"
import { getList as getPagesList } from "@/sequelize/getPage"

export async function GET() {
	const body = await render()

	const headers = {
    'Cache-Control': `max-age=0, s-max-age=${600}`,
    'Content-Type': 'application/xml'
  }

	return new Response(
		body.trim(),
		{ headers }
	)
}

const render = async () => {
	const [cars, types, posts, pages, brands] = await Promise.all([
		getList(),
		getTypes(),
		getPosts(),
		getPagesList(),
		getBrands()
	])

	let page = `
		<?xml version="1.0" encoding="UTF-8"?>
		<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
			xmlns:xhtml="http://www.w3.org/1999/xhtml">
			<url>
				<loc>https://dcr.ae</loc>
				<xhtml:link
					rel="alternate"
					hreflang="en"
					href="https://dcr.com"/>
				<xhtml:link
					rel="alternate"
					hreflang="ru"
					href="https://dcr.com/ru"/>
				<xhtml:link
					rel="alternate"
					hreflang="ar"
					href="https://dcr.com/ar"/>
				<lastmod>${dayjs("2022-12-29 13:41").format()}</lastmod>
				<changefreq>monthly</changefreq>
				<priority>1</priority>
			</url>
	`

	for (let {url} of pages) {
		page += `
			<url>
				<loc>https://dcr.ae/${url}</loc>
				<xhtml:link
					rel="alternate"
					hreflang="en"
					href="https://dcr.com/${url}"/>
				<xhtml:link
					rel="alternate"
					hreflang="ru"
					href="https://dcr.com/ru/${url}"/>
				<xhtml:link
					rel="alternate"
					hreflang="ar"
					href="https://dcr.com/ar/${url}"/>
				<lastmod>${dayjs("2022-12-29 13:41").format()}</lastmod>
				<changefreq>monthly</changefreq>
				<priority>${url === "cars" ? "1" : url === "frequently-asked-questions" ? "0.75" : url === "blog" ? "0.5" : "0.25"}</priority>
			</url>
		`
	}

	for (let list of [types, brands, cars]) {
		for (let {url} of list) {
			page += `
				<url>
					<loc>https://dcr.ae/cars/${url}</loc>
					<xhtml:link
						rel="alternate"
						hreflang="en"
						href="https://dcr.com/cars/${url}"/>
					<xhtml:link
						rel="alternate"
						hreflang="ru"
						href="https://dcr.com/ru/cars/${url}"/>
					<xhtml:link
						rel="alternate"
						hreflang="ar"
						href="https://dcr.com/ar/cars/${url}"/>
					<lastmod>${dayjs("2022-12-29 13:41").format()}</lastmod>
					<changefreq>monthly</changefreq>
					<priority>0.75</priority>
				</url>
			`
		}
	}

	for (let {url} of posts) {
		page += `
			<url>
				<loc>https://dcr.ae/blog/${url}</loc>
				<xhtml:link
					rel="alternate"
					hreflang="en"
					href="https://dcr.com/blog/${url}"/>
				<xhtml:link
					rel="alternate"
					hreflang="ru"
					href="https://dcr.com/ru/blog/${url}"/>
				<xhtml:link
					rel="alternate"
					hreflang="ar"
					href="https://dcr.com/ar/blog/${url}"/>
				<lastmod>${dayjs("2022-12-29 13:41").format()}</lastmod>
				<changefreq>monthly</changefreq>
				<priority>0.5</priority>
			</url>
		`
	}

	page += `
		</urlset>
	`

	return page
}