import "dotenv/config"

import dayjs from "dayjs"
import got from "got"

/** @type {import("./$types").RequestHandler} */
export async function GET(e) {
	const stream = got.stream(`https://${process.env.ADMIN_URL}/files/${e.params.slug}/${e.params.file}`)

	const headers = {
		'Expires': `${dayjs().add(1, "month").format("ddd, DD MMM YYYY HH:mm:ss")} GMT`,
		'Cache-Control': `public, max-age=${1000 * 60 * 60 * 24 * 30};`
	}

	return new Response(
		stream, 
		{ headers }
	)
}