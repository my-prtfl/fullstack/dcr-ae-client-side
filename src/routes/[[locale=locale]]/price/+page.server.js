import { getPrice } from "@/sequelize/getCars"
import { getMeta } from "@/sequelize/getMeta"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const crumbs = [ { text: "default.nav.price", i18n: true } ]

	const [price, meta] = await Promise.all([
		getPrice(), getMeta("price", "pages", ctx.params.locale)
	])

	return {
		title: "heading.pricelist",
		i18n: true,
		hideSelector: true,
		crumbs, price, meta
	}
}