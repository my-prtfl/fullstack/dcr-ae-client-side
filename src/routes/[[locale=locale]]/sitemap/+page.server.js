import { getList } from "@/sequelize/getCars"
import { getMeta } from "@/sequelize/getMeta"
import { getPosts } from "@/sequelize/getPosts"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const crumbs = [ { text: "default.nav.sitemap", i18n: true } ]

	const [meta, cars, posts] = await Promise.all([
		getMeta("sitemap", "pages", ctx.params.locale),
		getList(),
		getPosts()
	])

	return {
		title: "default.nav.sitemap",
		i18n: true,
		hideSelector: true,
		crumbs, cars, posts, meta
	}
}