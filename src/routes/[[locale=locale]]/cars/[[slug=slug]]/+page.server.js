import { error } from '@sveltejs/kit'

import { getMeta } from "@/sequelize/getMeta"
import { getCars, getCar } from "@/sequelize/getCars"

import { brands as brandsStore, types as typesStore } from "@/stores/static"
import { get } from "svelte/store"

const defineTable = slug => {
	if (!slug) return { table: "pages" }

	const brands = get(brandsStore)
	const inBrands = brands.find(({ url }) => url === slug)

	const types = get(typesStore)
	const inTypes = types.find(({ url }) => url === slug)

	return {
		table: inTypes ? "categories" : inBrands ? "brands" : "products",
		brand: inBrands,
		type: inTypes
	}
}

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	let types = ctx.url.searchParams.get("types")
	let brands = ctx.url.searchParams.get("brands")
	const price_min = ctx.url.searchParams.get("price_min")
	const price_max = ctx.url.searchParams.get("price_max")
	const order = ctx.url.searchParams.get("popular") || "DESC"

	const slug = ctx.params.slug
	const { table, brand, type } = defineTable(slug)

	const crumbs = [ { text: "default.nav.inventory", i18n: true, url: "cars", icon: "widgets" } ]

	if (slug) {
		if (table == "categories") {
			types = slug
			crumbs.push({ text: type.name })
		} else {
			brands = slug
			crumbs.push({ text: brand.name })
		}
	}
	
	const [ meta, cars ] = await Promise.all([
		getMeta(slug || "cars", table, ctx.params.locale),
		getCars({ types, brands, price_min, price_max, order })
	])

	return {
		title: "default.nav.inventory",
		i18n: true,
		crumbs,
		meta,
		cars
	}
}