import { error } from '@sveltejs/kit'

import { getMeta } from "@/sequelize/getMeta"
import { getCar } from "@/sequelize/getCars"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const url = ctx.params.car

	const [ meta, { car, more } ] = await Promise.all([
		getMeta(url, "products", ctx.params.locale),
		getCar(url, ctx.params.locale),
	])

	if (!car) throw error(404)

	const crumbs = [ 
		{ text: "default.nav.inventory", i18n: true, url: "cars", icon: "widgets" },
		{ text: car.name, url: car.url, icon: "bus" }
	]

	// let lowest
	// car.images.reduce((p, c, i) => {
	// 	if (c.position > p.position) {
	// 		return c
	// 	} else {
	// 		lowest = i
	// 		return p
	// 	}
	// })
	
	// car.images.splice(lowest, 1)

	const title = car.name

	return {
		car,
		more,
		meta,
		title,
		crumbs
	}
}