import { getMeta } from "@/sequelize/getMeta"
import { getPage } from "@/sequelize/getPage"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const crumbs = [ { text: "default.nav.faq", i18n: true } ]

	const [meta, body] = await Promise.all([
		getMeta("frequently-asked-questions", "pages", ctx.params.locale),
		getPage("frequently-asked-questions")
	])

	for (let locale in body) {
		let result = []

		const reg = /<h2>(.*?)<\/p>/gm
		for (let match of body[locale].match(reg)) {
			let title = /<h2>(.*?)<\/h2>/gm.exec(match)?.[1]
			let text = /<p>(.*?)<\/p>/gm.exec(match)?.[1]
		
			result.push({title, text})
		}

		body[locale] = result
	}

	return {
		title: "heading.faq",
		i18n: true,
		hideSelector: true,
		crumbs, body, meta
	}
}