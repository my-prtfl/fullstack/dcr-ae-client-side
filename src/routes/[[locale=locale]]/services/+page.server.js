import { getMeta } from "@/sequelize/getMeta"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const crumbs = [ { text: "default.nav.services", i18n: true } ]

	const meta = await getMeta("services", "pages", ctx.params.locale)

	return {
		title: "default.nav.services",
		i18n: true,
		crumbs, meta
	}
}