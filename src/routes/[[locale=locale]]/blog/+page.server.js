import { getMeta } from "@/sequelize/getMeta"
import { getPosts } from "@/sequelize/getPosts"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const crumbs = [ { text: "default.nav.blog", i18n: true } ]

	const [meta, posts] = await Promise.all([
		getMeta("blog", "pages", ctx.params.locale),
		getPosts()
	])

	return {
		title: "heading.blog",
		i18n: true,
		hideSelector: true,
		crumbs, posts, meta
	}
}