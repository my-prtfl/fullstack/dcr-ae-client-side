import { error } from '@sveltejs/kit'

import { getMeta } from "@/sequelize/getMeta"
import { getPost } from "@/sequelize/getPosts"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const [meta, {post, more}] = await Promise.all([
		getMeta(ctx.params.url, "blog", ctx.params.locale),
		getPost(ctx.params.url)
	])

	if (!post) throw error(404)

	const crumbs = [ 
		{ text: "default.nav.blog", i18n: true, url: "blog" },
		{ text: post.name[ctx.params.locale || "en"] }
	]
	
	return {
		title: post.name[ctx.params.locale || "en"],
		hideSelector: true,
		crumbs, post, more, meta
	}
}