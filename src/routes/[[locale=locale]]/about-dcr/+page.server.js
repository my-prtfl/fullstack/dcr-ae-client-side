import { getMeta } from "@/sequelize/getMeta"
import { getPage } from "@/sequelize/getPage"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const crumbs = [ { text: "default.nav.about_us", i18n: true } ]

	const [meta, body] = await Promise.all([
		getMeta("about-dcr", "pages", ctx.params.locale),
		getPage("about-dcr")
	])

	// for (let locale in body) {
	// 	console.log(body)
	// }

	return {
		title: "default.nav.about_us",
		i18n: true,
		hideSelector: true,
		crumbs, body, meta
	}
}