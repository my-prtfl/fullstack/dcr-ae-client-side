import useragent from "express-useragent"

import { getPopular } from "@/sequelize/getCars"
import { getMeta } from "@/sequelize/getMeta"

import { redirect } from '@sveltejs/kit'
import { l } from "@/i18n"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const ua = ctx.request.headers.get('user-agent')
	const parsed = useragent.parse(ua)

	let $l
	l.subscribe(v => $l = v)

	// if (parsed.isMobile)
	// 	throw redirect(301, $l("cars"))

	const [popular, meta] = await Promise.all([
		getPopular(),
		getMeta("", "pages", ctx.params.locale)
	])

	return { popular, meta }
}