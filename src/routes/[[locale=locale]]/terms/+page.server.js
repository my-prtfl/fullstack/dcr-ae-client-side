import { getMeta } from "@/sequelize/getMeta"
import { getPage } from "@/sequelize/getPage"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const crumbs = [ { text: "default.nav.terms", i18n: true } ]

	const [meta, body] = await Promise.all([
		getMeta("terms", "pages", ctx.params.locale),
		getPage("terms")
	])

	return {
		title: "default.nav.terms",
		i18n: true,
		hideSelector: true,
		crumbs, body, meta
	}
}