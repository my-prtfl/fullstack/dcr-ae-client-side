import { loadTranslations } from "@/i18n";
import localeChecker from "@/helpers/localeChecker"
import { brands as brandsStore, types as typesStore } from "@/stores/static"

/** @type {import('@sveltejs/kit').Load} */
export const load = async ctx => {
	const { types, brands } = ctx.data

	brandsStore.set(brands)
	typesStore.set(types)

	const locale = localeChecker(ctx)
	
	await loadTranslations(locale)
		
	return { ...ctx.data, locale }
}