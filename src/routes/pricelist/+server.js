/*const { Router } = require("express"),
			puppeteer = require('puppeteer'),
			stream = require("stream")*/

import puppeteer from "puppeteer"
import stream from "stream"

export async function GET(e) {
	const params = { headless: true }

	if (process.env.NODE_ENV == "production") {
		params.executablePath = process.env.EXECUTABLE_PATH
		params.args = [ "--no-sandbox" ]
	}

	const browser = await puppeteer.launch(params)
	const page = await browser.newPage()

	await page.goto('http://localhost:4173/price', {waitUntil: 'networkidle0'})
	
	const pdf = await page.pdf({ 
		format: 'A4',
		displayHeaderFooter: true,
		printBackground: true
	})

	await browser.close();
	
	const s = stream.Readable.from(pdf)

	return new Response(s)
}