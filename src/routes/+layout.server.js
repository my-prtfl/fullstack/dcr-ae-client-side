import useragent from "express-useragent"

import { get } from "svelte/store"
import { brands, types } from "@/stores/static"

/** @type {import('@sveltejs/kit').ServerLoad} */	
export async function load(ctx) {
	const ua = ctx.request.headers.get('user-agent')
	const parsed = useragent.parse(ua)

	if (process.env.NODE_ENV == "production" && ctx.url.host !== "dcr.ae")
		throw redirect(301, "https://dcr.ae")

	return {
		brands: get(brands),
		types: get(types),
		isMobile: parsed.isMobile,
		noLocalePath: ctx.url.pathname.replace(/\/(ar|ru)\/?/, "").replace("/", "")
	}
}

export const ssr = true