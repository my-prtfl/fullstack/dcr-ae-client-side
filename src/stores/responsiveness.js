import { writable } from "svelte/store"

export const isSmall = writable(false)
export const isPhone = writable(false)
export const isTablet = writable(false)
export const isDesktop = writable(false)
export const isWide = writable(false)
export const isLarge = writable(false)

const checkScreen = () => {
	if (!import.meta.env.SSR) {
		const w = window.innerWidth
		isSmall.set(w <= 550)
		isPhone.set(w <= 769)
		isTablet.set(w <= 1023 && w >= 769)
		isDesktop.set(w <= 1215 && w >= 1024)
		isWide.set(w >= 1216 && w <= 1407)
		isLarge.set(w >= 1408)
	}
}

if (!import.meta.env.SSR) {
	checkScreen()
	window.addEventListener("resize", checkScreen, { passive: true })
}