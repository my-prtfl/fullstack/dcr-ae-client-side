import axios from "axios"
import { writable, get } from "svelte/store"

export const pending = writable(false)
export const success = writable(false)

export const data = writable({
	brand: "",
	service: "",
	car: "",
	variant: "",
	name: "",
	phone: "",
	start: "",
	end: "",
	contact_by: {
		call: false,
		viber: false,
		telegram: false,
		whatsapp: false
	}
})

export const error = writable({
	invalidPhone: false,
	emptyPhone: false,
	emptyName: false,
	emptyStart: false,
	emptyEnd: false
})

const checkEmpty = () => {
	const errors = get(error)
	const { name, phone, start, end } = get(data)

	errors.emptyPhone = !phone
	errors.emptyName = !name
	errors.emptyStart = !start
	errors.emptyEnd = !end

	error.set(errors)
}

const hasErrors = () => {
	const errors = get(error)

	return !!Object.keys(errors).map(prop => errors[prop]).filter(item => item).length
}

const contact_variants = {
	call: "Phone call",
	viber: "Viber",
	telegram: "Telegram",
	whatsapp: "Whatsapp"
}

const parseBody = () => {
	let contact_by = ""

	for (let prop in get(data).contact_by) {
		if (get(data).contact_by[prop]) {
			if (contact_by)
				contact_by += ", " + get(data).contact_by[prop]
			else
				contact_by += get(data).contact_by[prop]
		}
	}

	return {
		...get(data),
		fullPhone: get(data).phone,
		brand: get(data).brand?.name,
		contact_by
	}
}

export const submit = async () => {
	if (get(pending)) return

	try {
		checkEmpty()
	
		if (hasErrors()) return
	
		pending.set(true)
		
		await axios.post("/request", parseBody())
		success.set(true)
	} catch(err) {
		console.error(err)
	}

	pending.set(false)
}