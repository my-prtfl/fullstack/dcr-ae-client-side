import { derived, writable } from "svelte/store"

export const brands = writable([])
export const types = writable([])

export const aside = writable(true)

export const theme = writable("dark")
export const isLight = derived(theme, $theme => $theme === "light")

aside.subscribe(val => {
	if (!import.meta.env.SSR) {
		if (val) {
			document.getElementsByTagName('html')[0].classList.add("is-clipped")
			window.scrollTo({
				top: 0,
				behavior: 'smooth'
			})
		} else {
			document.getElementsByTagName('html')[0].classList.remove("is-clipped")
	
		}
	}
})