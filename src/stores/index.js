import { writable } from "svelte/store"

export const modal = writable(false)

modal.subscribe(val => {
	if (import.meta.env.SSR) return

	const action = val ? "add" : "remove"
	const root = document.getElementsByTagName("html")[0]

	root.classList[action]("is-clipped")
})