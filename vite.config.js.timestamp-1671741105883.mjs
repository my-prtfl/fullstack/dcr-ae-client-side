// vite.config.js
import { fileURLToPath, URL } from "node:url";
import { sveltekit } from "file:///Users/uhomuho/Projects/DCR%20ae/client/node_modules/@sveltejs/kit/src/exports/vite/index.js";
var __vite_injected_original_import_meta_url = "file:///Users/uhomuho/Projects/DCR%20ae/client/vite.config.js";
var config = {
  plugins: [sveltekit()],
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", __vite_injected_original_import_meta_url))
    }
  },
  server: {
    port: 4173,
    strictPort: false
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@import "src/assets/scss/common/variables"; @import "src/assets/scss/common/mixins";'
      }
    }
  }
};
var vite_config_default = config;
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcuanMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCIvVXNlcnMvdWhvbXVoby9Qcm9qZWN0cy9EQ1IgYWUvY2xpZW50XCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ZpbGVuYW1lID0gXCIvVXNlcnMvdWhvbXVoby9Qcm9qZWN0cy9EQ1IgYWUvY2xpZW50L3ZpdGUuY29uZmlnLmpzXCI7Y29uc3QgX192aXRlX2luamVjdGVkX29yaWdpbmFsX2ltcG9ydF9tZXRhX3VybCA9IFwiZmlsZTovLy9Vc2Vycy91aG9tdWhvL1Byb2plY3RzL0RDUiUyMGFlL2NsaWVudC92aXRlLmNvbmZpZy5qc1wiO2ltcG9ydCB7IGZpbGVVUkxUb1BhdGgsIFVSTCB9IGZyb20gJ25vZGU6dXJsJ1xuXG5pbXBvcnQgeyBzdmVsdGVraXQgfSBmcm9tIFwiQHN2ZWx0ZWpzL2tpdC92aXRlXCJcblxuY29uc3QgY29uZmlnID0ge1xuICBwbHVnaW5zOiBbc3ZlbHRla2l0KCldLFxuXG4gIHJlc29sdmU6IHtcbiAgICBhbGlhczoge1xuICAgICAgJ0AnOiBmaWxlVVJMVG9QYXRoKG5ldyBVUkwoJy4vc3JjJywgaW1wb3J0Lm1ldGEudXJsKSlcbiAgICB9XG4gIH0sXG5cblx0c2VydmVyOntcblx0XHRwb3J0OiA0MTczLFxuXHRcdHN0cmljdFBvcnQ6IGZhbHNlXG5cdH0sXG5cbiAgY3NzOiB7XG4gICAgcHJlcHJvY2Vzc29yT3B0aW9uczoge1xuICAgICAgc2Nzczoge1xuICAgICAgICBhZGRpdGlvbmFsRGF0YTogJ0BpbXBvcnQgXCJzcmMvYXNzZXRzL3Njc3MvY29tbW9uL3ZhcmlhYmxlc1wiOyBAaW1wb3J0IFwic3JjL2Fzc2V0cy9zY3NzL2NvbW1vbi9taXhpbnNcIjsnXG4gICAgICB9LFxuICAgIH0sXG4gIH0sXG59O1xuXG5leHBvcnQgZGVmYXVsdCBjb25maWdcbiJdLAogICJtYXBwaW5ncyI6ICI7QUFBbVMsU0FBUyxlQUFlLFdBQVc7QUFFdFUsU0FBUyxpQkFBaUI7QUFGd0osSUFBTSwyQ0FBMkM7QUFJbk8sSUFBTSxTQUFTO0FBQUEsRUFDYixTQUFTLENBQUMsVUFBVSxDQUFDO0FBQUEsRUFFckIsU0FBUztBQUFBLElBQ1AsT0FBTztBQUFBLE1BQ0wsS0FBSyxjQUFjLElBQUksSUFBSSxTQUFTLHdDQUFlLENBQUM7QUFBQSxJQUN0RDtBQUFBLEVBQ0Y7QUFBQSxFQUVELFFBQU87QUFBQSxJQUNOLE1BQU07QUFBQSxJQUNOLFlBQVk7QUFBQSxFQUNiO0FBQUEsRUFFQyxLQUFLO0FBQUEsSUFDSCxxQkFBcUI7QUFBQSxNQUNuQixNQUFNO0FBQUEsUUFDSixnQkFBZ0I7QUFBQSxNQUNsQjtBQUFBLElBQ0Y7QUFBQSxFQUNGO0FBQ0Y7QUFFQSxJQUFPLHNCQUFROyIsCiAgIm5hbWVzIjogW10KfQo=
