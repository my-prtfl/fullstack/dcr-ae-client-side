FROM node:16-alpine as builder

RUN mkdir -p /usr/src/app && mkdir -p /app

WORKDIR /app

COPY package*.json ./
RUN npm ci
COPY . .

RUN npm run build

FROM node:16-alpine

WORKDIR usr/src/app

COPY --from=builder /app/build .
COPY --from=builder /app/package.json .
COPY --from=builder /app/node_modules ./node_modules

EXPOSE 3000

RUN apk update && apk upgrade && \
    apk add --no-cache git && apk add chromium
		
CMD ["node", "index.js"]