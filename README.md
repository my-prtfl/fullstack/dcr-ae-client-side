# DCR.ae

> New version DCR [website](https://dcr.ae)

## Stack
* [Sveltekit](https://kit.svelte.dev/)
* [Bulma](https://bulma.io)
* MySQL
* Sequelize

## Functional:
* Manipulation with database using Sequelize (Without models)
* Fetching images from adminpanel (SEO task says that all images url must stay the same, so i made [this](https://gitlab.com/my-prtfl/fullstack/dcr-ae-client-side/-/blob/main/src/routes/files/%5Bslug%5D/%5Bfile%5D/%2Bserver.js))