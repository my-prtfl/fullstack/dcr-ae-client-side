import { fileURLToPath, URL } from 'node:url'

import { sveltekit } from "@sveltejs/kit/vite"

const config = {
  plugins: [sveltekit()],

  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },

	server:{
		port: 4173,
		strictPort: false
	},

  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@import "src/assets/scss/common/variables"; @import "src/assets/scss/common/mixins";'
      },
    },
  },
};

export default config
