import preprocess from "svelte-preprocess";
import adapter from "@sveltejs/adapter-node";

/** @type {import('@sveltejs/kit').Config} */
const config = {
  kit: {
    adapter: adapter({ 
      out: 'build',
      precompress: {
        brotli: true,
        gzip: true,
        files: ["html", "js", "css"]
      }
    })
  },

  preprocess: [
    preprocess({
      scss: {
        prependData: '@import "src/assets/scss/common/variables"; @import "src/assets/scss/common/mixins";'
      },
    }),
  ]
}

export default config;
